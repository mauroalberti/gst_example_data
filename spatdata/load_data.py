
import traceback

from gst.core.geometries.grids.rasters import *
from gst.core.geometries.lines import *

from gst.io.rasters.gdal_io import *
from gst.io.vectors.ogr_io import *


current_folder = os.path.dirname(__file__)


def read_dem(
    area: str
) -> Tuple[Union[type(None), Grid], Error]:

    try:

        if area == "timpa_san_lorenzo":
            src_dem_pth = f"{current_folder}/../data_sources/timpa_san_lorenzo/tsl_tinitaly_w84u32.tif"
        elif area == "valnerina":
            src_dem_pth = f"{current_folder}/../data_sources/valnerina/valnerina_utm33_red.tif"
        else:
            return None, Error(
                True,
                caller_name(),
                Exception(f"Provided area is {area}, that is not an expected value"),
                traceback.format_exc()
            )

        result, err = read_raster_band_with_projection(
            raster_source=src_dem_pth
        )

        if err:
            return None, err

        geotransform, epsg, band_params, data, gdal_projection = result

        return Grid(
            array=data,
            geotransform=geotransform,
            epsg_code=epsg,
            projection=gdal_projection
        ), Error()

    except Exception as e:

        return None, Error(
            True,
            caller_name(),
            e,
            traceback.format_exc()
        )


def read_profile(
    area: str
) -> Tuple[Union[type(None), Ln], Error]:

    try:

        if area == "timpa_san_lorenzo":
            src_profile = f"{current_folder}/../data_sources/timpa_san_lorenzo/profile2.shp"
        elif area == "valnerina":
            src_profile = f"{current_folder}/../data_sources/valnerina/profile.shp"
        else:
            return None, Error(
                True,
                caller_name(),
                Exception(f"Provided area is {area}, that is not an expected value"),
                traceback.format_exc()
            )

        profiles, err = read_line_shapefile_with_attributes(
            shp_path=src_profile)

        if err:
            return None, err

        ([([profile_line, ], _)], epsg_code) = profiles

        if area == "timpa_san_lorenzo":
            profile_line = profile_line.reversed()

        return profile_line, Error()

    except Exception as e:

        return None, Error(
            True,
            caller_name(),
            e,
            traceback.format_exc()
        )


def read_faults(
    area: str
) -> Tuple[Union[type(None), Dict], Error]:

    try:

        if area == "timpa_san_lorenzo":
            faults_shape = f"{current_folder}/../data_sources/timpa_san_lorenzo/faults.shp"
        elif area == "valnerina":
            faults_shape = f"{current_folder}/../data_sources/valnerina/faults.shp"
        else:
            return None, Error(
                True,
                caller_name(),
                Exception(f"Provided area is {area}, that is not an expected value"),
                traceback.format_exc()
            )

        result, err = read_line_shapefile_via_ids(
            shp_path=faults_shape,
            id_field_name="id"
        )

        if err:
            return None, err

        _, _, faults_dict = result

        return faults_dict, Error()

    except Exception as e:

        return None, Error(
            True,
            caller_name(),
            e,
            traceback.format_exc()
        )


def read_outcrops(
    area: str
):

    try:

        if area == "timpa_san_lorenzo":
            src_polygon_shpfl_path = f"{current_folder}/../data_sources/timpa_san_lorenzo/geological_outcrops_utm32N.shp"
        elif area == "valnerina":
            src_polygon_shpfl_path = f"{current_folder}/../data_sources/valnerina/geological_outcrops.shp"
        else:
            return None, Error(
                True,
                caller_name(),
                Exception(f"Provided area is {area}, that is not an expected value"),
                traceback.format_exc()
            )

        result, err = read_polygon_shapefile_via_ids(
            shp_path=src_polygon_shpfl_path,
            id_field_name="code"
        )

        if err:
            return None, err

        epsg_cd, layer_extent, polygons = result

        return polygons, Error()

    except Exception as e:

        return None, Error(
            True,
            caller_name(),
            e,
            traceback.format_exc()
        )


def read_lines_with_attitudes(
    area: str
):

    try:

        if area == "timpa_san_lorenzo":

            src_shapefile = f"{current_folder}/../data_sources/timpa_san_lorenzo/merged_intersections.shp"

        elif area == "valnerina":

            src_shapefile = f"{current_folder}/../data_sources/valnerina/main_plane_269_06_7.shp"

        else:

            return None, Error(
                True,
                caller_name(),
                Exception(f"Provided area is {area}, that is not an expected value"),
                traceback.format_exc()
            )

        if area == "timpa_san_lorenzo":

            results, err = read_line_shapefile_with_attributes(
                shp_path=src_shapefile,
                flds=["Source", "dip_dir", "dip_ang"]
            )

            if err:
                return None, err

            lines_data, _ = results

            line_orientations = defaultdict(list)
            for lines, (source, dip_dir, dip_ang) in lines_data:
                line_orientations[source].append([Plane(dip_dir, dip_ang), lines])

            return line_orientations, Error()

        elif area == "valnerina":

            results, err = read_line_shapefile(
                shp_path=src_shapefile,
            )

            if err:
                return None, err

            lines, epsg_code = results

            return {"main plane 269-06.7": (Plane(269, 6.7), lines)}, Error()

    except Exception as e:

        return None, Error(
            True,
            caller_name(),
            e,
            traceback.format_exc()
        )


def load(
    area: str = 'timpa_san_lorenzo',
    type: str = 'DEM'
) -> Tuple[Union[type(None), List[Ln], Dict, Grid], Error]:

    try:

        if type == 'DEM':

            return read_dem(
                area=area
            )

        elif type == "profile":

            return read_profile(
                area=area
            )

        elif type == "outcrops":

            return read_outcrops(
                area=area
            )

        elif type == "faults":

            return read_faults(
                area=area
            )

        elif type == "lines_with_attitudes":

            return read_lines_with_attitudes(
                area=area
            )

    except Exception as e:

        return None, Error(
            True,
            caller_name(),
            e,
            traceback.format_exc()
        )





