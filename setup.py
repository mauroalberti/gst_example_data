#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

requirements = [
        'numpy',
        'matplotlib',
        'geopandas',
        'setuptools',
    ]

with open("README.md", "r") as readme_file:
    readme = readme_file.read()

setup(
    name='spatdata',
    version='0.0.1',
    author="Mauro Alberti",
    author_email="alberti.m65@gmail.com",
    description="spatdat, a library of geospatial structural data",
    long_description=readme,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/mauroalberti/spatdata.git",
    project_urls={
        "Bug Tracker": "https://gitlab.com/mauroalberti/spatdata/-/issues",
        "Documentation": "https://gitlab.com/mauroalberti/spatdata/-/blob/master/README.md",
        "Source Code": "https://gitlab.com/mauroalberti/spatdata/-/tree/master",
    },
    packages=find_packages(),
    install_requires=requirements,
    license="CC BY-NC-SA",
    classifiers=[
                   "Operating System :: OS Independent",
                   "Intended Audience :: Developers",
                   "Intended Audience :: Science/Research",
                   "Programming Language :: Python :: 3",
                   "Programming Language :: Python :: 3 :: Only",
                   "Topic :: Software Development",
                   "Topic :: Scientific/Engineering :: GIS",
                   "Topic :: Scientific/Engineering :: Structural Geology",
                   "Topic :: Utilities"
               ],
)